ADDRESS FORMAT : XY (in hexadecimal 0-f)
X = modular growth unit (1-6)
Y = Peripheral devices
  0 = temp sensor
  1 = pH sensor
  2 = empty (light sensor?)
  3 = solenoid gas valve
  4 = water movement motor
  5 = peristaltic pump
  6-f = empty

Adresses 08-0f --> General purpose addresses (e.g. common adjustment for all leds)
08 = LED light sensor
09 = LED light servo
Adresses 70-77 --> Extra adresses for bells and whistles
